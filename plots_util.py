import matplotlib.pyplot as plt
import numpy as np
from yahoo_parser import Portfolio


def plot_all(x: Portfolio, inleg=None, days=None, pie_delta=None):
    summary(x)
    if days:
        portfolio(x, inleg, days)
        profit(x, days)
    portfolio(x, inleg)
    profit(x)
    portfolio_pie(x, pie_delta)
    sectors(x)
    currency(x)
    div_per_month(x)
    div_sum(x)
    div_ttm(x)
    div_ttm_ph(x)

def summary(x):
    print('Value:          ' + str(int(x.portfolio_value.iloc[-1])))
    print('Yield:          ' + str(int(10000*np.sum(x.forward_dividend)/float(x.portfolio_value.iloc[-1]))/100) + '%')
    print('Anual Dividend: ' + str(int(np.sum(x.forward_dividend))))

def portfolio(x, inleg, days=None):
    plt.figure(figsize=(15, 5))
    if days:
        plt.plot(x.dates[-days:], x.price_paid[-days:], label='Inleg')
        plt.plot(x.dates[-days:], x.portfolio_value[-days:], label='Waarde portfolio')
        # plt.plot(x.dates[-days:], x.portfolio_value_incldiv[-days:], label='Waarde portfolio incl dividends')
    else:
        plt.plot(x.dates, x.price_paid, label='Inleg')
        plt.plot(x.dates, x.portfolio_value, label='Waarde portfolio')
        # plt.plot(x.dates, x.portfolio_value_incldiv, label='Waarde portfolio incl dividends')
    if inleg:
        plt.axhline(y=inleg, ls='--')
    plt.axhline(y=np.amax(x.portfolio_value.to_numpy()), ls='--', color='g')
    plt.tick_params(labelright=True)
    plt.xticks(rotation=45)
    plt.grid(True)
    plt.title('Portfolio')
    plt.legend()
    plt.show()
    # print(np.amax(x.portfolio_value.to_numpy()))


def profit(x, days=None):
    plt.figure(figsize=(15, 5))
    if days:
        plt.plot(x.dates[-days:], x.portfolio_value.to_numpy()[-days:] - x.price_paid.to_numpy()[-days:], label='Winst')
        plt.plot(x.dates[-days:], x.portfolio_value.to_numpy()[-days:] - x.price_paid.to_numpy()[-days:] + x.dividends_per_date_sum.to_numpy()[-days:], label='Winst inlc. dividend')
    else:
        plt.plot(x.dates, x.portfolio_value.to_numpy() - x.price_paid.to_numpy(), label='Winst')
        plt.plot(x.dates, x.portfolio_value.to_numpy() - x.price_paid.to_numpy() + x.dividends_per_date_sum.to_numpy(), label='Winst incl. dividend')
    # plt.axhline(y=0, ls='--')
    plt.axhline(y=np.amax(x.portfolio_value.to_numpy() - x.price_paid.to_numpy()), ls='--')
    plt.axhline(y=np.amax(x.portfolio_value.to_numpy() - x.price_paid.to_numpy() + x.dividends_per_date_sum.to_numpy()), ls='--', color='orange')
    plt.tick_params(labelright=True)
    plt.xticks(rotation=45)
    plt.grid(True)
    plt.title('Winst')
    plt.legend()
    plt.show()
    # print(np.amax(x.portfolio_value.to_numpy() - x.price_paid.to_numpy()))


def portfolio_pie(x, pie_delta=None):
    if pie_delta:
        x.portfolio_value_summary = x.calculate_portfolio_value_summary(pie_delta)
    plt.figure(figsize=(10, 10))
    plt.pie(x.portfolio_value_summary, labels=x.portfolio_value_summary.index,
            pctdistance=0.9, autopct='%1.1f%%', startangle=180, rotatelabels=False,
            colors=plt.get_cmap('Pastel1').colors)
    plt.show()


def sectors(x):
    plt.figure(figsize=(10, 10))
    plt.pie(x.sectors, labels=x.sectors.index,
            pctdistance=0.8, autopct='%1.1f%%', startangle=180, rotatelabels=False,
            colors=plt.get_cmap('Pastel1').colors)
    circle = plt.Circle((0, 0), 0.6, color='white')
    p = plt.gcf()
    p.gca().add_artist(circle)
    # plt.text(0,0,'test')
    plt.show()


def currency(x):
    plt.figure(figsize=(10, 10))
    plt.pie(x.currency_distribution, labels=x.currency_distribution.index,
            pctdistance=0.5, autopct='%1.1f%%', startangle=90, colors=plt.get_cmap('Pastel2').colors)
    plt.show()


def div_per_month(x):
    x.dividends_per_month.plot.bar(rot=0, figsize=(15, 5), title='Dividends')
    plt.axhline(y=float(np.sum(x.forward_dividend)/12), ls='--', color='g')
    plt.axhline(y=float(x.dividends_ttm.iloc[-1]), ls='--', color='r')
    plt.tick_params(labelright=True)
    plt.grid(axis='y')
    plt.show()
    # print(np.sum(x.dividends_per_date)[0])


def div_sum(x):
    plt.figure(figsize=(15, 5))
    plt.bar(x.dividends_sum.index, x.dividends_sum['Sum'])
    plt.tick_params(labelright=True)
    plt.title('Totaal dividend')
    plt.xticks(rotation=45)
    plt.grid(axis='y')
    plt.show()


def div_ttm(x):
    plt.figure(figsize=(15, 5))
    plt.bar(x.dividends_ttm.index, x.dividends_ttm['TTM'])
    plt.xticks(rotation=45)
    plt.tick_params(labelright=True)
    plt.title('Gemiddelde dividend per maand van de afgelopen 12 maanden')
    plt.grid(axis='y')
    plt.show()


def div_ttm_ph(x):
    plt.figure(figsize=(15, 5))
    plt.bar(x.dividends_ttm.index, 4.2 * (x.dividends_ttm['TTM'] / 30.5 / 24), label='40u werkweek')
    plt.plot(x.dividends_ttm.index, x.dividends_ttm['TTM'] / 30.5 / 24, 'r-', label='per uur')
    plt.xticks(rotation=45)
    plt.tick_params(labelright=True)
    plt.title('Dividend per uur')
    plt.grid(axis='y')
    try:
        plt.legend()
    except:
        pass
    plt.show()