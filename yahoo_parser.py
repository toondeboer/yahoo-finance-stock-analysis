import pandas as pd
import numpy as np
from datetime import *
import yfinance as yf
from tqdm import tqdm


class Portfolio:
    def __init__(self, input='data/quotes.csv', dividends_input=None, output='info.csv', download=False):
        self.input = input
        self.dividends_input = dividends_input
        self.output = output
        self.download = download

        self.lots = self.__read_file()
        self.custom_dividends = self.__read_file_dividends()
        self.symbols = list(dict.fromkeys(self.lots['Symbol'].values.tolist()))
        self.info = self.__download_info()
        self.start_date = self.lots['Trade Date'][0] - timedelta(days=1)
        self.dates = np.arange(self.start_date, date.today()+timedelta(days=1), timedelta(days=1)).astype(date)

        self.exchange_rates = self.__download_exchange_rates()
        self.yahoo_data = self.__download_yahoo()

        self.quantities = self.__add_quantities()
        self.price_paid = self.__calculate_price_paid()
        self.market_values = self.__calculate_market_value()
        self.portfolio_value = self.__calculate_portfolio_value()

        self.dividends = self.__calculate_dividends()
        self.dividends = self.__add_custom_dividends()
        self.dividends_per_date = self.__calculate_dividends_per_date()
        self.dividends_per_date_sum = self.__calculate_dividends_per_date_sum()

        self.dividends_per_month = self.__calculate_dividends_per_month()
        self.dividends_per_month_sequential = self.__calculate_dividends_per_month_sequential()
        self.dividends_sum = self.__calculate_dividends_sum()
        self.dividends_ttm = self.__calculate_dividends_ttm()
        self.portfolio_value_summary = self.calculate_portfolio_value_summary(0.01)
        self.sectors = self.__calculate_sectors()
        self.currency_distribution = self.__calculate_currency_distribution()

        self.forward_dividend = self.__calculate_forward_dividend()

        pd.set_option("display.max_rows", None, "display.max_columns", None)

    def __read_file(self):
        df = None

        for i in self.input:
            quotes = pd.read_csv(i)
            data = pd.DataFrame(quotes)
            data = data.drop(columns=['Current Price', 'Date', 'Time', 'Change', 'Open', 'High', 'Low', 'Volume',
                                      'Commission', 'High Limit', 'Low Limit', 'Comment'])
            if df is None:
                df = data
            else:
                df = df.append(data)

        df = self.__sort_on_buy_date(df)
        return df

    def __read_file_dividends(self):
        if self.dividends_input is not None:
            divs = pd.read_csv(self.dividends_input)
            data = pd.DataFrame(divs)

            df = self.__sort_on_buy_date(data)
            return df

        return None

    def __sort_on_buy_date(self, df):
        df['Trade Date'] = df['Trade Date'].apply(self.__parse_date)
        df = df.sort_values('Trade Date')
        df = df.reset_index()
        return df.drop(columns=['index'])

    def __parse_date(self, string):
        year = str(string)[:4]
        month = str(string)[4:6]
        day = str(string)[6:]
        return date.fromisoformat(year + '-' + month + '-' + day)

    def __download_info(self):
        output = self.output
        indices = []
        values = np.array([])
        if not self.download:
            try:
                data = pd.read_csv(output, index_col=0)
                indices = data.index.to_numpy()
                values = data.to_numpy()
            except:
                pass

        for t in tqdm(self.symbols):
            if t not in indices:
                try:
                    info = yf.Ticker(t).info
                    name = info['longName']
                    try:
                        sector = info['sector']
                    except:
                        sector = info['quoteType']
                    currency = info['currency']
                    dividend = info['dividendRate']
                except:
                    print('No information about ' + t)
                    name = t
                    sector = 'ETF'
                    print(t + ' sector set to ETF')
                    currency = 'EUR'
                    print(t + ' currency set to EUR')
                    dividend = None
                if len(values) > 0:
                    values = np.vstack((values, np.array([name, sector, currency, dividend])))
                else:
                    values = np.array([name, sector, currency, dividend])
                indices = np.append(indices, t)

        df = pd.DataFrame(values, index=indices, columns=['Name', 'Sector', 'Currency', 'Dividend'])
        df.to_csv(output)

        try:
            data = pd.read_csv(output, index_col=0)
            indices = data.index.to_numpy()
            values = data.to_numpy()
        except:
            pass
        df = pd.DataFrame(values, index=indices, columns=['Name', 'Sector', 'Currency', 'Dividend'])

        return df

    def __download_exchange_rates(self):
        dates = self.dates
        currencies = ['EUR=X', 'GBPEUR=X']
        data = yf.download(currencies, start=self.start_date + timedelta(days=1), end=date.today() + timedelta(days=1))
        exchange_rates = np.array([])

        data
        index = 0
        next_yahoo = data.iloc[0]
        for d in dates:
            if d == next_yahoo.name:
                if len(exchange_rates) > 0:
                    next_rates = next_yahoo['Close'].to_numpy()

                    for r in range(len(next_rates)):
                        if not next_rates[r] > 0:
                            if exchange_rates[-1].shape == ():
                                next_rates[r] = exchange_rates[r]
                            else:
                                next_rates[r] = exchange_rates[-1, r]

                    exchange_rates = np.vstack((exchange_rates, next_rates))
                else:
                    exchange_rates = next_yahoo['Close'].to_numpy()
                index += 1
                if index < len(data.index):
                    next_yahoo = data.iloc[index]
            else:
                if len(exchange_rates) > 0:
                    if exchange_rates.ndim > 1:
                        exchange_rates = np.vstack((exchange_rates, exchange_rates[-1]))
                    else:
                        exchange_rates = np.vstack((exchange_rates, exchange_rates))
                else:
                    exchange_rates = np.zeros(len(currencies))

        for index in range(len(exchange_rates)):
            i = len(exchange_rates) - index - 1
            for r in range(len(exchange_rates[i])):
                if not exchange_rates[i, r] > 0:
                    exchange_rates[i, r] = exchange_rates[i + 1, r]

        df = pd.DataFrame(exchange_rates, index=self.dates, columns=['USD', 'GBP'])
        return df

    def __download_yahoo(self):
        data = yf.download(self.symbols, start=self.start_date + timedelta(days=1), end=date.today()+timedelta(days=1), actions=True)
        data = data.drop(columns=['Adj Close', 'Open', 'High', 'Low', 'Volume', 'Stock Splits'])

        for s in self.symbols:
            if not s in data['Dividends']:
                data['Dividends', s] = 0
        return data

    def __add_quantities(self):
        symbols = self.symbols
        dates = self.dates
        df = pd.DataFrame(np.zeros((len(dates), len(symbols))), index=dates, columns=symbols)

        for i, lot in self.lots.iterrows():
            new_quantity = df.loc[str(lot['Trade Date']), lot['Symbol']].item() + lot['Quantity']
            if new_quantity < 1e-6:
                df.loc[np.arange(lot['Trade Date'], date.today()+timedelta(days=1), timedelta(days=1)).astype(date), lot['Symbol']] = 0
            else:
                df.loc[np.arange(lot['Trade Date'], date.today()+timedelta(days=1), timedelta(days=1)).astype(date), lot['Symbol']] = new_quantity

        df.index = pd.to_datetime(df.index)
        return df

    def __calculate_price_paid(self):
        dates = self.dates
        lots = self.lots
        info = self.info
        exchange_rates = self.exchange_rates.to_numpy()

        prices_paid = np.array([])
        next_price = 0
        index = 0
        lot = lots.iloc[0]
        for d in range(len(dates)):
            if len(prices_paid) > 0:
                next_price = prices_paid[-1]
            if index < len(lots):
                while lot['Trade Date'] <= datetime.date(dates[d]):
                    currency = info['Currency'][lot['Symbol']]
                    if currency == 'EUR':
                        next_price += lot['Purchase Price'] * lot['Quantity']
                    elif currency == 'USD':
                        next_price += lot['Purchase Price'] * exchange_rates[d,0] * lot['Quantity']
                    elif currency == 'GBP':
                        next_price += lot['Purchase Price'] * exchange_rates[d,1] * lot['Quantity']
                    elif currency == 'GBp':
                        next_price += lot['Purchase Price'] * exchange_rates[d,1] * lot['Quantity']/100
                    else:
                        next_price += lot['Purchase Price'] * lot['Quantity']
                        print(currency + ' not implemented')
                    index += 1
                    if index >= len(lots):
                        break
                    lot = lots.iloc[index]
            prices_paid = np.append(prices_paid, next_price)

        df = pd.DataFrame(prices_paid, index=dates, columns=['Price Paid'])

        return df

    def __calculate_market_value(self):
        dates = self.dates
        symbols = self.symbols
        info = self.info
        quantities = self.quantities.to_numpy()
        yahoo_data = self.yahoo_data
        market_values = np.zeros(len(symbols))
        exchange_rates = self.exchange_rates.to_numpy()

        index = 0
        next_yahoo = yahoo_data.iloc[0]
        if next_yahoo.name == dates[0]:
            next_yahoo = yahoo_data.iloc[1]
            index = 1
        for d in range(1, len(dates)):
            if dates[d] == next_yahoo.name:
                next_market_values = np.array([])
                exchange_rate = exchange_rates[d]
                for s in range(len(symbols)):
                    value = next_yahoo['Close', symbols[s]]
                    if value > 0:
                        currency = info['Currency'][symbols[s]]
                        if currency == 'EUR':
                            next_market_values = np.append(next_market_values, value * quantities[d, s])
                        elif currency == 'USD':
                            next_market_values = np.append(next_market_values, value * exchange_rate[0] * quantities[d, s])
                        elif currency == 'GBP':
                            next_market_values = np.append(next_market_values, value * exchange_rate[1] * quantities[d, s])
                        elif currency == 'GBp':
                            next_market_values = np.append(next_market_values, value * exchange_rate[1] * quantities[d, s]/100)
                        else:
                            next_market_values = np.append(next_market_values, value * quantities[d, s])
                            print(currency + ' not implemented')
                    else:
                        if d > 1 and quantities[d-1,s] > 0:
                            next_market_values = np.append(next_market_values,
                                                           market_values[d-1, s] * (quantities[d, s]/quantities[d - 1, s]))
                        else:
                            next_market_values = np.append(next_market_values, 0)

                market_values = np.vstack((market_values, next_market_values))

                index += 1
                if index < len(yahoo_data.index):
                    next_yahoo = yahoo_data.iloc[index]
            else:
                market_values = np.vstack((market_values, market_values[-1]))

        for d in range(len(dates) - 1):
            index = len(dates) - d - 1
            values = market_values[index]
            for v in range(len(values)):
                if values[v] == 0 and quantities[index, v]:
                    if index == len(dates) - 1 or quantities[index+1, v] == 0:
                        lot = self.lots.iloc[self.lots[self.lots['Symbol'] == self.symbols[v]].index[0]]
                        currency = info['Currency'][lot['Symbol']]
                        if currency == 'EUR':
                            market_values[index, v] = lot['Purchase Price'] * lot['Quantity']
                        elif currency == 'USD':
                            market_values[index, v] = lot['Purchase Price'] * exchange_rates[d,0] * lot['Quantity']
                        elif currency == 'GBP':
                            market_values[index, v] = lot['Purchase Price'] * exchange_rates[d, 1] * lot['Quantity']
                        elif currency == 'GBp':
                            market_values[index, v] = lot['Purchase Price'] * exchange_rates[d, 1] * lot['Quantity']/100
                        else:
                            market_values[index, v] = lot['Purchase Price'] * lot['Quantity']
                            print(currency + ' not implemented')
                        print(lot['Symbol'] + '=' + symbols[v] + ' - market value: ' + str(market_values[index, v])
                              + ' (purchase price: ' + str(lot['Purchase Price']) +', currency: ' + currency + ', quantity: '
                              + str(lot['Quantity']) + ')')
                    else:
                        market_values[index, v] = market_values[index+1, v] * (quantities[index, v]/quantities[index + 1, v])

        df = pd.DataFrame(market_values, index=dates, columns=symbols)
        return df

    def __calculate_portfolio_value(self):
        portfolio_value = np.sum(self.market_values.to_numpy(), axis=1)
        df = pd.DataFrame(portfolio_value, index=self.dates, columns=['Portfolio Value'])
        return df

    def __calculate_dividends(self):
        dates = self.dates
        symbols = self.symbols
        info = self.info
        quantities = self.quantities.to_numpy()
        yahoo_data = self.yahoo_data
        dividends = np.zeros(len(symbols))
        exchange_rates = self.exchange_rates.to_numpy()

        index = 0
        next_yahoo = yahoo_data.iloc[0]
        if next_yahoo.name == dates[0]:
            next_yahoo = yahoo_data.iloc[1]
            index = 1
        for d in range(1, len(dates)):
            if dates[d] == next_yahoo.name:
                next_dividend = np.array([])
                exchange_rate = exchange_rates[d]
                for s in range(len(symbols)):
                    value = next_yahoo['Dividends', symbols[s]]
                    if value > 0:
                        currency = info['Currency'][symbols[s]]
                        if currency == 'EUR':
                            next_dividend = np.append(next_dividend, value * quantities[d-1, s])
                        elif currency == 'USD':
                            next_dividend = np.append(next_dividend, value * exchange_rate[0] * quantities[d-1, s])
                        elif currency == 'GBP':
                            next_dividend = np.append(next_dividend, value * exchange_rate[1] * quantities[d-1, s])
                        elif currency == 'GBp':
                            next_dividend = np.append(next_dividend, value * exchange_rate[1] * quantities[d-1, s]/100)
                        else:
                            next_dividend = np.append(next_dividend, value * quantities[d-1, s])
                            print(currency + ' not implemented')
                    else:
                        next_dividend = np.append(next_dividend, 0)

                dividends = np.vstack((dividends, next_dividend))

                index += 1
                if index < len(yahoo_data.index):
                    next_yahoo = yahoo_data.iloc[index]
            else:
                dividends = np.vstack((dividends, np.zeros(len(symbols))))

        df = pd.DataFrame(dividends, index=dates, columns=symbols)
        return df

    def __add_custom_dividends(self):
        if self.custom_dividends is None:
            return self.dividends

        dates = self.dates
        custom_dividends = self.custom_dividends
        df = self.dividends
        div_symbols = list(self.custom_dividends.columns)[1:]

        index = 0
        next_div = custom_dividends.iloc[0]
        for d in range(0, len(dates)):
            if datetime.date(dates[d]) == next_div['Trade Date']:
                for s in div_symbols:
                    if next_div[s] > 0:
                        if df.iloc[d][s] > 0:
                            print("WARNING, DIVIDEND EXISTS ALREADY, PLEASE CHECK: " + s)
                        # if np.sum(dividends[s]) > 0:
                        #     print("WARNING, DIVIDEND IS IN PROBABLY IN YAHOO FOR THIS SYMBOL: " + s)
                        df.iloc[d][s] += next_div[s]
                index += 1
                if index < len(custom_dividends.index):
                    next_div = custom_dividends.iloc[index]

        return df

    def __calculate_dividends_per_date(self):
        dividends = np.sum(self.dividends.to_numpy(), axis=1)
        df = pd.DataFrame(dividends, index=self.dates, columns=['Dividends'])
        return df

    def __calculate_dividends_per_date_sum(self):
        dividends = np.array([])
        div_sum = 0
        for d in range(len(self.dividends_per_date)):
            div_sum += np.array(self.dividends_per_date)[d]
            dividends = np.append(dividends,  div_sum)

        df = pd.DataFrame(dividends, index=self.dates, columns=['Dividends Sum'])
        return df

    def __calculate_dividends_per_month(self):
        dividends = self.dividends_per_date
        months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        years = np.arange(self.start_date.year, date.today().year + 1)
        dividends_per_month = np.array([])

        index = 0
        next_div = dividends.iloc[0]

        for y in years:
            next_year = np.array([])

            for m in range(len(months)):
                next_month = 0
                while next_div.name.year == y and next_div.name.month == m+1 and index < len(dividends.index):
                    next_month += next_div['Dividends']
                    index += 1
                    if index < len(dividends.index):
                        next_div = dividends.iloc[index]
                next_year = np.append(next_year, next_month)

            if len(dividends_per_month) > 0:
                dividends_per_month = np.vstack((dividends_per_month, next_year))
            else:
                dividends_per_month = next_year

        df = pd.DataFrame(dividends_per_month.T, index=months, columns=years)
        return df

    def __calculate_dividends_per_month_sequential(self):
        months = np.array([])
        values = np.array([])
        today = datetime.today()
        for y in self.dividends_per_month.iteritems():
            for m in range(12):
                v = y[1][m]
                if (v > 0 or len(months) > 0) and (m < today.month or y[0] < today.year):
                    months = np.append(months, y[1].index[m] + '-' + str(y[0]))
                    values = np.append(values, v)

        df = pd.DataFrame(values, index=months, columns=['Dividends'])
        return df

    def __calculate_dividends_sum(self):
        values = np.array([])
        value = 0
        for i, v in self.dividends_per_month_sequential.iterrows():
            value += v
            values = np.append(values, value)

        df = pd.DataFrame(values, index=self.dividends_per_month_sequential.index, columns=['Sum'])
        return df

    def __calculate_dividends_ttm(self):
        ttm = np.array([])
        ttm_average = np.array([])
        for month, value in self.dividends_per_month_sequential.iterrows():
            if len(ttm) >= 12:
                ttm = ttm[:-1]

            ttm = np.append(value[0], ttm)

            ttm_average = np.append(ttm_average, ttm.sum() / 12)

        df = pd.DataFrame(ttm_average, index=self.dividends_per_month_sequential.index, columns=['TTM'])
        return df

    def calculate_portfolio_value_summary(self, m=0.01):
        a = self.market_values.iloc[-1]
        b = self.portfolio_value.iloc[-1][0]
        info = self.info

        others = 0
        values = {}
        if b > 0:
            for i in range(len(a)):
                if a[i] / b > m:
                    values[info['Name'][a.index[i]]] = a[i]
                else:
                    others += a[i]

        if others > 0:
            values['Others'] = 0

        df = pd.DataFrame(values.values(), index=values.keys(), columns=['Value'])
        df = df.iloc[:, 0].sort_values()
        if others > 0:
            df['Others'] = others
        return df

    def __calculate_sectors(self):
        values = self.market_values.iloc[-1]
        info = self.info

        sectors = {}
        for i in range(len(values)):
            sector = info['Sector'][values.index[i]]
            if sector in sectors:
                sectors[info['Sector'][values.index[i]]] += values[i]
            elif values[i] > 0:
                sectors[info['Sector'][values.index[i]]] = values[i]

        df = pd.DataFrame(sectors.values(), index=sectors.keys(), columns=['Value'])
        df = df.iloc[:, 0].sort_values()
        return df

    def __calculate_currency_distribution(self):
        values = self.market_values.iloc[-1]
        info = self.info

        currencies = {}
        for i in range(len(values)):
            c = info['Currency'][values.index[i]].upper()
            if c in currencies:
                currencies[c] += values[i]
            elif values[i] > 0:
                currencies[c] = values[i]

        df = pd.DataFrame(currencies.values(), index=currencies.keys(), columns=['Value'])
        df = df.iloc[:, 0].sort_values()
        return df

    def __calculate_forward_dividend(self):
        symbols = self.symbols
        info = self.info
        quantities = self.quantities.iloc[-1].to_numpy()
        exchange_rate = self.exchange_rates.iloc[-1].to_numpy()

        dividends = np.array([])
        new_symbols = np.array([])

        for s in range(len(symbols)):
            d = info['Dividend'][symbols[s]]
            if d > 0 and quantities[s] > 0:
                new_symbols = np.append(new_symbols, symbols[s])
                currency = info['Currency'][symbols[s]]
                if currency == 'EUR':
                    dividends = np.append(dividends, float(d) * quantities[s])
                elif currency == 'USD':
                    dividends = np.append(dividends, float(d) * exchange_rate[0] * quantities[s])
                else:
                    dividends = np.append(dividends, float(d) * quantities[s])
                    print(currency + ' not implemented')

        df = pd.DataFrame(dividends, index=new_symbols, columns=['Dividend'])
        return df.sort_values('Dividend', ascending=False)






